// access using dns
service-name.namespace-name.svc.cluster.local
my-httpd.default.svc.cluster.local

// describe
kc describe service service-name

// mocks external service
https://gorest.co.in/public/v2/users

// check service
minikube service service-name